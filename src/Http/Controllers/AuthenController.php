<?php
namespace RRT\Authen\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthenController extends Controller
{
    public function showLoginForm()
    {
        return view('rrt-authen::login');
    }

    public function login(Request $request)
    {
        // Xử lý logic login ở đây
    }
}