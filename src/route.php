<?php
use Illuminate\Support\Facades\Route;
Route::group(['prefix' => 'rrt-admin', 'namespace' => 'YourNamespace\\RrtAuthen\\Http\\Controllers'], function () {
    Route::get('authen/login', 'AuthenController@showLoginForm')->name('admin.authen.login');
    Route::post('authen/login', 'AuthenController@login')->name('admin.authen.login.post');
});