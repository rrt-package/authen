<form action="{{ route('admin.authen.login.post') }}" method="POST">
    @csrf
    <label for="email">Email:</label>
    <input type="email" id="email" name="email">
    <label for="password">Password:</label>
    <input type="password" id="password" name="password">
    <button type="submit">Login</button>
</form>