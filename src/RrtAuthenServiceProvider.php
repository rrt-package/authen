<?php

namespace RRT\Authen;

use Illuminate\Support\ServiceProvider;

class RrtAuthenServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'rrt-authen');
    }

    public function register()
    {
        //
    }
}